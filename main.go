package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"
)

type Repos struct {
	Repos []Repo
	//Auth bool
}

type Repo struct {
	Id int `json:"id"`
	Repository string `json:"name"`
	Commits int
}

type Commits struct {
	Id string `json:"id"`
}

const URLCOMMITS = "https://git.gvk.idi.ntnu.no/api/v4/projects/"
const PAGEURL = "https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page="

var repoArray = &[]Repo{}

func quicksort(a []Repo) []Repo {
	if len(a) < 2 {return a}
	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i].Commits > a[right].Commits {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}
	a[left], a[right] = a[right], a[left]
	quicksort(a[:left])
	quicksort(a[left +1:])
	return a
}

func CommitHandler(w http.ResponseWriter, r *http.Request) {

	http.Header.Add(w.Header(), "Content-type", "application/json")
	//URLParts := strings.Split(r.URL.Path, "/")

	for i := 1; i <= 2; i++ {
		response, err := http.Get(PAGEURL + strconv.Itoa(i))
		var restRepo = &[]Repo{}
		if err != nil {
			log.Fatal(err)
		}
		_ = json.NewDecoder(response.Body).Decode(restRepo)
		*repoArray = append(*repoArray, *restRepo...)
	}
	for i := range *repoArray{
		getId := (*repoArray)[i].Id
		stringId := strconv.Itoa(getId)
		response1, err := http.Get(URLCOMMITS + stringId + "/repository/commits")
		if err != nil {
			log.Fatal(err)
		}
		var restCommits = &[]Commits{}
		_ = json.NewDecoder(response1.Body).Decode(restCommits)
		commits := response1.Header.Get("X-Total")
		(*repoArray)[i].Commits, err = strconv.Atoi(commits)
		if err != nil {
			log.Fatal(err)
		}
	}

	quicksort(*repoArray)
	_ = json.NewEncoder(w).Encode(repoArray)

}

func handleRequests() {

	http.HandleFunc("/repocheck/v1/commits", CommitHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func main() {
	handleRequests()
}